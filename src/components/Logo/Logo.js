import React from "react";
import Tilt from "react-tilt";
import "./Logo.css";
import logoImage from "./Logo.png";

const Logo = () => {
  return (
    <div className="ma4 mt0">
      <Tilt
        className="Tilt br2 shadow-2 pointer"
        options={{ max: 50 }}
        style={{ height: 160, width: 150 }}
      >
        <div className="Tilt-inner pt2 pb0">
          <img src={logoImage} alt="logo" />
          <p className="white f5"> IOThings Hackaton</p>
        </div>
      </Tilt>
    </div>
  );
};

export default Logo;
