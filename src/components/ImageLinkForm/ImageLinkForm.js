import React from "react";
import "./ImageLinkForm.css";

class ImageLinkForm extends React.Component {
  onDeviceOpen = () => {
    fetch("http://iothings.centralus.cloudapp.azure.com:3005/poweron", {
      method: "post"
    });
  };
  onDeviceClose = () => {
    fetch("http://iothings.centralus.cloudapp.azure.com:3005/poweroff", {
      method: "post"
    });
  };
  render() {
    return (
      <div>
        <div className="center f1">
          <div className="center form pa4 br1 shadow-5 mv5">
            <button
              onClick={this.onDeviceOpen}
              className="w-30 grow f4 link ph1 pv2 dib white bg-blue pointer br4 shadow-2 btnBG mh4"
            >
              Abrir
            </button>
            <button
              onClick={this.onDeviceClose}
              className="w-30 grow f4 link ph1 pv2 dib white bg-blue pointer br4 shadow-2 btnBG mh4"
            >
              Cerrar
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default ImageLinkForm;
